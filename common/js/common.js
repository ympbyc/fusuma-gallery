document.createElement('main');

$(function(){
    smoothScroll();
});

/*-- ユーザーエージェント関連 --*/
var g_userAgent = window.navigator.userAgent.toLowerCase(),
    g_appVersion = window.navigator.appVersion.toLowerCase(),
    g_isIe8 = false;


//IE判定
$(function() {
  if (g_userAgent.indexOf("msie") != -1) {
    if (g_appVersion.indexOf("msie 8.") != -1) {
      $("body").addClass("ie8");
      g_isIe8 = true;
    }
  }

  if(g_userAgent.indexOf("mac") != -1) {
    $("body").addClass("mac");
  } else if(g_userAgent.indexOf("windows") != -1) {
    $("body").addClass("windows");
  }
});

//スムーススクロール
function smoothScroll(){
  $('a[href^=#]').not(".noScroll").click(function() {
    var speed = 400; // ミリ秒(この値を変えるとスピードが変わる)
    var href = $(this).attr("href");
    var target = $(href == "#" || href == "" ? 'html' : href);
    var position = target.offset().top; //targetの位置を取得
    var body = 'body';
    if (g_userAgent.indexOf('msie') > -1 || g_userAgent.indexOf('trident') > -1 || g_userAgent.indexOf("firefox") > -1 ) { /*IE6.7.8.9.10.11*/
      body = 'html';
    }
    $(body).animate({
      scrollTop: position
    }, speed, 'swing');
    return false;
  });
}

$(function() {

  //nav control
  $("#headerLogoToggle a").click(function() {
    $("body").toggleClass("navActive");
    return false;
  });
  $(".navBg").click(function() {
    $("body").removeClass("navActive");
    return false;
  });
  $("#globalNavi > ul > li:not(.others) > a:not([target='_blank'])").click(function() {
    if($(window).width() <= 768) {
      $(this).next("ul").slideToggle(200);
      return false;
    }
  });

  $("#gNavFaceSearch a").click(function() {
    $("#globalNavi #searchForm").addClass("active");
    $("body").prepend("<div id='bgSearch'></div>");
    $("#bgSearch").css({
      "width": $(window).width(),
      "height": $(window).height(),
      "background": "#fff",
      "opacity": "0.01",
      "position": "fixed",
      "left": "0",
      "top": "0",
      "z-index": "3"
    });
    return false;
  });
  $(document).on("click", "#bgSearch", function(){
    $("#bgSearch").hide();
    $("#globalNavi #searchForm").removeClass("active");
  });


  //pageTop control
  var defPos = 0;
  if($(window).scrollTop() <= 400) {
    $("#pageTop").fadeOut();
  }
  $(window).scroll(function(){
    var currentPos = $(this).scrollTop();
    if (currentPos - defPos > 0) {
      if($(window).scrollTop() > 400) {
        $("#pageTop:not(:animated)").fadeOut();
      }
    } else {
        $("#pageTop:not(:animated)").fadeIn();
    }
    defPos = currentPos;
  });


  //メニューのFix
  $(window).on("load scroll touchmove", function(){
    if($(window).scrollTop() > 100){
      if(!$("body").hasClass("menuFixed")) $("body").addClass("menuFixed");
    } else {
      if($("body").hasClass("menuFixed")) $("body").removeClass("menuFixed");    }
  });


  //tab panel

  $tabHistory = sessionStorage.getItem("tab_history");
  var currentUrl = location.href;
  currentUrl = currentUrl.split("#");
  currentUrl = currentUrl[0];
  if($tabHistory)
    $tabHistory = JSON.parse($tabHistory);
  else
    $tabHistory = [];
  $(".tabMenu a").on("click", function() {
    var tgHref = $(this).attr("href"),
        $tgElm = $(tgHref),
        tabIndex = $(this).closest(".tabMenu").attr("data-index"),
        isRecorded = false;
    for(var i = 0; i < $tabHistory.length; i++) {
      if($tabHistory[i].url == currentUrl && $tabHistory[i].index == tabIndex) {
        $tabHistory[i].target = tgHref;
        isRecorded = true;
      }
    }
    if(!(isRecorded)) {
      $tabHistory.push({
        "url": currentUrl,
        "index": tabIndex,
        "target": tgHref
      });
    }
    sessionStorage.setItem("tab_history", JSON.stringify($tabHistory));
    $tgElm.parent().parent().find(".tabMenu a").each(function() {
      $(this).removeClass("active");
      $($(this).attr("href"))
        .removeClass("active");
    });
    $(this).addClass("active");
    $tgElm
      .addClass("active");
    return false;
  });
  $(".tabMenu").each(function(index) {
    $(this).attr("data-index", index);
    for(var i = 0; i < $tabHistory.length; i++) {
      if($tabHistory[i].url == currentUrl && $tabHistory[i].index == index) {
        $(this).find("a[href='" + $tabHistory[i].target + "']").trigger("click");
      }
    }
  });

  //area map responsive
  if($('img[usemap]').size() > 0) {
    $('img[usemap]').rwdImageMaps();
  }

  //scrollbar custom
  if(!g_isIe8) {
    $('.scrollbar').perfectScrollbar();
  }


  //side navi control
  $("#sideNavi dt").click(function() {
    $(this).toggleClass("active");
  });


  //pager view
  if($(".paginationNav").size() > 0) {
    $(".paginationNav .pager").addClass("spDisabled");
    $(".paginationNav .pager").last().removeClass("spDisabled");
    var currentPagerIndex = $(".paginationNav .pager").index($(".paginationNav .current")),
        pagerSize = $(".paginationNav .pager").size();
    if(currentPagerIndex == 0) currentPagerIndex = 1;
    if(currentPagerIndex == pagerSize - 1) currentPagerIndex = pagerSize - 2;
    for(var i = currentPagerIndex - 1; i < currentPagerIndex + 2; i++) {
      $(".paginationNav .pager").eq(i).removeClass("spDisabled");
    }
    if(pagerSize > 3 && $(".paginationNav .pager").index($(".paginationNav .current")) < pagerSize - 2) {
      $(".paginationNav .pager").last().before("<span class='pager interval'><span>…</span></span>");
    } else if(pagerSize > 3) {
      $(".paginationNav .pager").eq(pagerSize - 4).removeClass("spDisabled");
    }
    $(".paginationNav .interval").addClass("pcDisabled");
  }


  //slider
  $(window).load(function(){
    var windowWidth = $(window).width();
    if(windowWidth > 1080) var setCenterPadding = Math.floor((windowWidth - 1040) / 2) + "px";
    else var setCenterPadding = Math.floor((windowWidth - 289) / 2) + "px"
    var $pickupSlider = $('.sliderWrapper ul').slick({
      slidesToShow: 3,
      centerMode: true,
      focusOnSelect: true,
      centerPadding: setCenterPadding
    });
    $(".sliderControler .prev").click(function() {
      $($pickupSlider).slick("slickPrev");
    });
    $(".sliderControler .next").click(function() {
      $($pickupSlider).slick("slickNext");
    });
    if(windowWidth > 1080) {
      $(".sliderControler .prev").css("left", 0);
      $(".sliderControler .next").css("right", 0);
    } else {
      $($pickupSlider).slick("slickSetOption", "slidesToShow",  1, true);
      $($pickupSlider).slick("slickSetOption", "centerPadding", Math.floor((windowWidth - 261) / 2) + "px", true);
      $(".sliderControler .prev").css("left", ((windowWidth - 261) / 2 - 10) + "px");
      $(".sliderControler .next").css("right", ((windowWidth - 261) / 2  - 10) + "px");
    }

    $(window).on("resize", function() {
      var windowWidth = $(window).width();
      if(windowWidth > 1080) {
        $($pickupSlider).slick("slickSetOption", "slidesToShow",  3, true);
        $($pickupSlider).slick("slickSetOption", "centerPadding", Math.floor((windowWidth - 1040) / 2) + "px", true);
        $(".sliderControler .prev").css("left", 0);
        $(".sliderControler .next").css("right", 0);
      } else {
        $($pickupSlider).slick("slickSetOption", "slidesToShow",  1, true);
        $($pickupSlider).slick("slickSetOption", "centerPadding", Math.floor((windowWidth - 261) / 2) + "px", true);
        $(".sliderControler .prev").css("left", ((windowWidth - 261) / 2 - 10) + "px");
        $(".sliderControler .next").css("right", ((windowWidth - 261) / 2 - 10) + "px");
      }
    });
  });

  //次の10件を表示
  if($(".nextPostsShow").size() > 0) {
    $(".nextPostsShow").each(function() {
      var $thisElm = $(this),
          $thisTrigger = $(this).children("a"),
          $tgElm = $($thisTrigger.attr("href"));
      $(window).on("resize", hideEntries);

      function hideEntries() {
        if($(window).width() <= 786) {
          $tgElm.find("dt:nth-of-type(n+11), dd:nth-of-type(n+11)").not(".showed").hide();
        } else {
          $tgElm.find("dt, dd").show();
        }
      } hideEntries();

      $thisTrigger.click(function() {
        $showTgElms = $tgElm.find("dt:hidden, dd:hidden");
        for(var i = 0; i < 20 && i < $showTgElms.size(); i++) {
          $showTgElms.eq(i).show().addClass("showed");
        }
        if($tgElm.find("dt:hidden, dd:hidden").size() == 0) $thisElm.hide();
        return false;
      });
    });
  }

  //height settings
  if($(".pageNav").size() > 0) {
    $(".pageNav").each(function() {
      $(this).find("a").fixHeightSimple({
        boxSizingBorderBox: true
      });
    });
  }
  if($(".faqLinksSection").size() > 0) {
    $(".faqLinksSection").each(function() {
      $(this).find("li").fixHeightSimple({
        responsive: true,
        responsiveParent: ".faqLinksSection ul",
        boxSizingBorderBox: true
      });
    });
  }
  if($(".officialsListSection dl dd").size() > 0) {
    $(".officialsListSection dl dd").each(function() {
      $(this).find("li a").fixHeightSimple({
        responsive: true,
        responsiveParent: ".officialsListSection dl dd",
        boxSizingBorderBox: true
      });
    });
  }


  //Swipebox
  $("a[href$='.jpg']:not([target]), a[href$='.png']:not([target]), a[href$='.gif']:not([target]), area[href$='.jpg'], area[href$='.png'], area[href$='.gif'], .swipebox-video").swipebox();

});
