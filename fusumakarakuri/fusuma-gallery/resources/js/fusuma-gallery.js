(function () {
    var request = window.superagent;
    var K = window.kakahiaka;
    var thumbs = "./fusuma-gallery/resources/imgs/fusuma-thumbs/";
    var bodyR = null;
    var fusuma_cv;
    var loupe_cv;
    var picon;
    var cant_load = false;

    //load test
    var load_test_timeout = setTimeout(function () {
        cant_load = true;
    }, 8000);
    new THREE.TextureLoader().load(
        encodeURI(thumbs+"鳳凰/01.png"),
        function onLoad (texture) {
            clearTimeout(load_test_timeout);
            cant_load = false;
        });

    Number.prototype.mod = function(n) {
        return ((this%n)+n)%n;
    };

    var app = K.app({
        index: null,
        playing: false,
        fusumas: [{
            num:    12,
            era:    "明治",
            area:   "神領　青井夫",
            artist: "西宮戒龍門",
            front:  {name: "竜虎",
                     info: "時代物"},
            back:   {name: "松に鶴",
                     info: "時代物"}
        }, {
            num:    11,
            era:    "江戸",
            area:   "神領　上角名",
            artist: "不明",
            front:  {name: "中千畳",
                     info: "時代物"},
            back:   {name: "鳳凰",
                     info: "時代物"}
        }, {
            num:    8,
            era:    "昭和",
            area:   "上分北谷地区",
            artist: "泉　青光",
            front:  {name: "鷲",
                     info: "時代物 國性爺合戦 獅子ヶ城棲門の段<br/>(メモ) 上分校卒業生 泉青光作 大世話人 山田熊蔵"},
            back:   {name: "森林と瀑布",
                     info: "時代物<br/>昭和12年11月作"}
        }, {
            num:    12,
            era:    "明治",
            area:   "神領　中津　大埜地名",
            artist: "不明",
            front:  {name: "金地富士松",
                     info: "時代物"},
            back:   {name: "金地一匹龍",
                     info: "時代物 絵本太功記 鷲森の段 祇園祭禮信仰記 金閣寺の段"}
        }, {
            num:    9,
            era:    "明治",
            area:   "下分　左右内地区",
            artist: "不明",
            front:  {name: "城",
                     info: "時代物　仮名手本忠臣蔵　城明渡しの段"},
            back:   {name: "花模様",
                     info: "時代物"}
        }, {
            num:    8,
            era:    "明治",
            area:   "阿野　五反地名",
            artist: "不明",
            front:  {name: "笹に鶴",
                     info: "玉藻前旭袂　化粧殿の段"},
            back:   {name: "鷲_",
                     info: "時代物"}
        }]});
    window.fusuma_app = app;

    var load_fusumas = K.deftransition(function (st, names) {
        return {fusumas: _.map(names, x => ({name: x[0], num: x[1]}))};
    });

    var inc = K.deftransition(function (st) {
        return {index: (st.index+1).mod(st.fusumas.length)};
    });

    var dec = K.deftransition(function (st) {
        return {index: (st.index-1).mod(st.fusumas.length)};
    });

    document.addEventListener("DOMContentLoaded", function () {
        /*request.get("./fusuma-gallery/resources/imgs/fusuma-thumbs/dlist.txt")
            .type("text/plain")
            .end((err, res) => {
                load_fusumas(app, _.splat(res.text.split("\n"), 2));
            });
        */
        fusuma_cv = document.querySelector("#fusuma-canvas");
        loupe_cv = document.querySelector("#fusuma-loupe-canvas");

        if (window.innerWidth < 400)
            thumbs = "./fusuma-gallery/resources/imgs/fusuma-thumbs-small/";

        picon = document.getElementById("play-icon");
        picon.addEventListener("click", e=> freeze=false);

        $(document).on("click", "[data-index]", function (e) {
            try {
                K.simple_update(app, 'index', parseInt(e.currentTarget.dataset.index));
            } catch (err) {
                K.simple_update(app, 'index', parseInt(e.currentTarget.attributes["data-index"].nodeValue));
            }
        });

        document.getElementById("left-icon").addEventListener("click", e=>{
            dec(app);
        });
        document.getElementById("right-icon").addEventListener("click", e=>{
            inc(app);
        });
        $(document).on("keyup", ".remodal", e=>{
            if (e.keyCode == 37)
                dec(app);
            else if (e.keyCode  == 39)
                inc(app);
            else if (e.keyCode  == 38)
                freeze  = false;
        });
        var visibilityToggle = {visible: "hidden", hidden:"visible"};
        document.getElementById("fusuma-zoom").addEventListener("click", e=>{
            if (isSmallScreen()) zoomed = !zoomed;
            else 
                loupe_cv.style.visibility = visibilityToggle[getComputedStyle(loupe_cv).visibility];
        });

        $(document).on("opened", ".remodal", function () {
           try {adapt_size();}catch(err){} 
        });
        $(document).on("closed", ".remodal", function () {
           loupe_cv.style.visibility = "hidden";
        });

        $(window).on("orientationchange resize", adapt_size);


        
        var loupe_rect = {width:200, height:200};
        fusuma_cv.addEventListener("mousemove", loupe_move);
        loupe_cv .addEventListener("mousemove", e=> {
            var tgt=e.target;
            loupe_move({
                layerX: parseFloat(tgt.style.left) + (e.offsetX || e.layerX),
                layerY: parseFloat(tgt.style.top) + (e.offsetY || e.layerY),
                target: fusuma_cv
            });
        });
        loupe_cv.addEventListener("wheel", e=> {
            loupe_cam.zoom -= e.deltaY*0.001;
            loupe_cam.updateProjectionMatrix();
        });
        loupe_cv.addEventListener("click", e=> freeze = !freeze);
        fusuma_cv.addEventListener("click", e=> freeze = !freeze);
        /*fusuma_cv.addEventListener("touchmove", e=> {
            var touch = e.touches[0];
            loupe_move({
                layerX: touch.pageX,
                layerY: touch.pageY,
                target: fusuma_cv
            });
        });*/

        function loupe_move (e) {
            var mX = e.offsetX || e.layerX;
            var mY = e.offsetY || e.layerY;
            if ( ! window.requestAnimationFrame) return;
            requestAnimationFrame(()=>{
                var cv = e.target;
                var rect = cv.getBoundingClientRect();
                var pos = loupe_cam.position;
                loupe_cam.position.set((mX-rect.width/2)*0.01, -(Math.min(rect.height,mY)-rect.height/2-25)*0.008, pos.z);
                loupe_cv.style.left = (mX - loupe_rect.width/2)+"px";
                loupe_cv.style.top = Math.min(rect.height-loupe_rect.height/2, (mY - loupe_rect.height/2))+"px";
            });
        }


        three_init();
    });

    K.watch_transition(app, "index", function (st) {
        if (_.isNull(st.index)) return;
        try {
            if (cant_load) throw "can't load";
            view3d(st.fusumas[st.index]);
        } catch (err) {
            //console.log(err);
            err_fallback(st.fusumas[st.index]);
        }
    });

    K.watch_transition(app, "fusumas", function (st) {
        return;
        var wrap = document.querySelector("#fusumas");
        _.each(st.fusumas, (fsm, i) => {
            var dv = make_thumb(fsm);
            wrap.appendChild(dv);
            if (i == 0) dv.onclick();
        });
    });

    function make_thumb (fsm) {
        var dv = document.createElement("div");
        var ovl = document.createElement("div");
        ovl.className = "fusuma-thumb-overlay";
        dv.className = "fusuma-thumb";
        dv.style.backgroundImage = "url('" + thumbs + fsm.front.name + "/thumb.png')";
        var spn = document.createElement("span");
        spn.innerHTML = fsm.front.name;
        dv.appendChild(ovl);
        ovl.appendChild(spn);
        dv.onclick = ()=>view3d(fsm);
        return dv;
    }

    function fill_info (fsm) {
        var wrap = document.getElementById("fusuma-info");
        _.each(wrap.querySelectorAll(".fusuma-detail"), (el)=>{
            el.innerHTML = el.dataset ? fsm[el.dataset.detail] : fsm[$(el).data("detail")];
        });
    }

    var renderer;
    var loupe_renderer;
    var scene;
    var camera;
    var loupe_cam;
    var composer;
    var amb_light;
    function three_init () {
        renderer = new THREE.WebGLRenderer({
            canvas: fusuma_cv,
            antialias: true,
            preserveDrawingBuffer: true
        });
        loupe_renderer = new THREE.WebGLRenderer({
            canvas: loupe_cv,
            antialias: true,
            preserveDrawingBuffer: true
        });
        renderer.setPixelRatio(window.devicePixelRatio);
        loupe_renderer.setPixelRatio(window.devicePixelRatio);
        loupe_renderer.setSize(200,200);
        scene = new THREE.Scene();
        scene.background = new THREE.Color( 0xffffff);
        camera = new THREE.PerspectiveCamera(45, 3.3, 1, 10000);
        camera.position.set(0, 0, +3);
        loupe_cam = new THREE.PerspectiveCamera(45, 1, 1, 10000);
        loupe_cam.position.set(-3, 0.5, +3);
        loupe_cam.zoom = 2.5;
        camera.updateProjectionMatrix();
        loupe_cam.updateProjectionMatrix();
        amb_light = new THREE.AmbientLight(0x404040);
        amb_light.intensity = 0;
        scene.add(amb_light);
        var dlight = new THREE.DirectionalLight(0xffffff);
        dlight.position.set(1,1,1);
        scene.add(dlight);

        composer = new THREE.EffectComposer(loupe_renderer);
        var renderPass = new THREE.RenderPass(scene, loupe_cam);
        composer.addPass(renderPass);
        window.loupe_composer = composer;
        var shaderPass = new THREE.ShaderPass(THREE.BrightnessContrastShader);
        composer.addPass(shaderPass);
        shaderPass.uniforms.contrast.value = 0.2;
        shaderPass.renderToScreen = true;
    }

    function fusuma_mesh (fsm, x, z, i) {
        const plane = new THREE.PlaneGeometry(1, 2.4);
        const mat = new THREE.MeshStandardMaterial({
            color: 0xffffff
        });
        var mesh = new THREE.Mesh(plane, mat);
        document.getElementById("loading-text").style.display = "block";
        if (i=0) var to = setTimeout(()=>{err_fallback(fsm)}, 20000);
        new THREE.TextureLoader().load(
            encodeURI(thumbs+fsm.name+"/"+('00'+x).slice(-2)+".png"),
            function onLoad (texture) {
                if (i=0) clearTimeout(to);
                texture.flipY = true;
                texture.wrapS = THREE.RepeatWrapping;
                texture.repeat.x = - 1;
                mesh.material = new THREE.MeshStandardMaterial({
                    map: texture
                });
                mesh.material.side = THREE.BackSide;
            },
            function onProgress () {
            },
            function onError (err) {
                throw err;
            }
        );
        mesh.translateZ(z);
        return mesh;
    }

    function fusuma_edge (x) {
        const plane = new THREE.PlaneGeometry(0.05, 2.4);
        const mat = new THREE.MeshStandardMaterial({color: new THREE.Color(0x000000)});
        mat.side = THREE.DoubleSide;
        mat.transparent = true;
        mat.opacity = 0.5;
        const mesh = new THREE.Mesh(plane, mat);
        mesh.translateX(x);
        mesh.rotateY(Math.PI/2);
        return mesh;
    }

    function adapt_size () {
        setTimeout(()=>{
            bodyR = document.querySelector("#fusuma-canvas-wrap").getBoundingClientRect();
            renderer.setSize(bodyR.width, bodyR.width * 0.3);
        },10);
    }

    var next = null;  //[Fn]
    var meshes = []; //[{}]
    var josho = [];
    var layern = -1;
    var freeze = true;
    var zoomed = false;
    var running = false;
    function view3d (fsm) {
        (function () {
            adapt_size();
            fill_info(_.assoc(fsm, 'name', fsm.back.name.replace("_", ""),
                                   'info', fsm.back.info));

            freeze = true;
            //setTimeout((()=> freeze=true), 1);
            
            if (!running) {
                running = true;
                requestAnimationFrame(anim);
            }
        })();

        next = {
            fsm: fsm,
            gen: ()=>{
                return _.map(_.range(1,fsm.num+1), (x,i) => {
                    var g = new THREE.Group();
                    var front = fusuma_mesh(_.assoc(fsm,"name", fsm.front.name), x, -0.025, i);
                    front.name="front";
                    front.material.transparent = true;
                    front.material.opacity = 0;
                    var back  = fusuma_mesh(_.assoc(fsm,"name", fsm.back.name), x, 0.025);
                    back.name="back";
                    back.material.transparent = true;
                    back.material.opacity = 0;
                    back.rotateY(Math.PI);
                    g.add(fusuma_edge(-0.5));
                    g.add(fusuma_edge(0.5));
                    g.add(front);
                    g.add(back);
                    g.translateX((fsm.num-x) - fsm.num / 2 + 0.5);
                    g.translateZ((layern++)*-0.001);
                    g.rotateZ((Math.random()-0.5)/30);
                    scene.add(g);
                    g.updateMatrix();
                    return g;
                });
            }
        };
    }

    function nearEq(x,y,torelance=0.1){
        return Math.abs(x-y) < torelance;
    }

    var theta = -1;
    var rot = 0;
    var jpos = 0;
    var clk = 0;
    var fsm;
    var dim = 1;
    var nopause = false; // dont pause on the first rotation

    function loadNext () {
        if (!_.isEmpty(josho)) return;
        //_.each(josho, x=>scene.remove(x));
        josho = meshes;
        fsm = next.fsm;
        meshes = next.gen();
        next = null;
        jpos = 0;
        dim = -1;
        document.getElementById("loading-text").style.display = "block";
        //nopause = true;
    }

    var anim = function () {
        requestAnimationFrame(anim);

        clk++;

        //regardless of freeze
        if (dim > 0) {
            jpos += 0.1;
            if (jpos > 3 && ! _.isEmpty(josho)) {
                _.each(josho, m=>scene.remove(m));
                josho = [];
                if (freeze) theta = -1;
            }
            _.each(josho, m=>{
                m.translateY(0.1);
            });
            _.each(meshes, g=>{
                g.getObjectByName("back").material.opacity+=0.01;
                g.getObjectByName("front").material.opacity+=0.01;
            });
        }

        amb_light.intensity = Math.min(5, Math.max(0, amb_light.intensity + 0.1 * dim));
        if (clk % 30 == 0 && dim != 1 && _.every(meshes, g => g.getObjectByName("back").material.map.image.naturalWidth > 10)) {
            dim = 1;
            document.getElementById("loading-text").style.display = "none";
        }

        if (freeze) {
            renderer.render(scene, camera);
            composer.render();
            $(".hide-in-play").css("opacity", "1").attr("disabled", false);
            if (next) {
                loadNext();
                setTimeout(()=>theta =-1, 0);
            }
            return;
        }
        var rot = 2*(Math.sin(2.5*Math.sin(theta)))+1.9;

        _.each(meshes, g=>{
            g.rotation.y = rot;
        });

        //load next layer
        if ((nearEq(rot, 0, 0.01)
             ||nearEq(rot, Math.PI, 0.01) )
            && next) loadNext();

        if (clk % 20 == 0 && fsm) {
            $(".hide-in-play").css("opacity", "0.2").attr("disabled", true);
            if (nearEq(rot, Math.PI, 1.5))
                fill_info(_.assoc(fsm, 'name', fsm.front.name.replace("_",""),
                                       'info', fsm.front.info));
            else
                fill_info(_.assoc(fsm, 'name', fsm.back.name.replace("_",""),
                                       'info', fsm.back.info));
        }
        if (nearEq(theta % (Math.PI*2), 4, 0.005)
            || (nearEq(theta % (Math.PI*2), 1.7, 0.005))) {
            if (nopause) nopause = false;
            else {
                freeze = true;
            }
        }


        if (_.isEmpty(josho)) theta += 0.01;

        if (zoomed && camera.position.z > 2)
            camera.position.z -= 0.02;
        else if (!zoomed && camera.position.z < 3)
            camera.position.z += 0.02;

        renderer.render(scene, camera);
        composer.render();
    };

    //requestAnimationFrame(anim);


    //fusuma image preload
    /*
    var imgArray = _.flatMap(
        K.deref(app).fusumas, 
        fsm => _.flatMap(
            _.range(1,fsm.num+1), 
            i=> [fsm.back.name.replace('_',"")  +'/'+('00'+i).slice(-2)+'.png', 
                 fsm.front.name.replace('_',"") +'/'+('00'+i).slice(-2)+'.png']))
    function loopImageLoader (i) {
        var image1 = new Image();
        var loaded = false;
        image1.crossOrigin = "Anonymous";
        image1.src = encodeURI(thumbs + imgArray[i]);
        image1.onload = function () {
            if (!loaded && (i +1 < imgArray.length)) {
                loaded = true;
                loopImageLoader(i+1);
            }
        }
        setTimeout(function () {
            if (!loaded && (i +1 < imgArray.length)) {
                loaded = true;
                loopImageLoader(i+1);
            }
        }, 1000)
    }
    try {
        loopImageLoader(0);
    } catch (err) {}
    */


    function err_fallback(fsm) {
        var cv = document.getElementById("fusuma-canvas");
        if (cv) cv.style.display = "none";
        var wrap = document.getElementById("fusuma-canvas-wrap");
        wrap.innerHTML = "";
        var div = make_image(fsm, document.createElement("div"), "back");
        wrap.appendChild(div);
        div.style.height = div.clientWidth / 8 * 2.3 + "px";
        fill_info(_.assoc(fsm, 'name', fsm.back.name.replace("_", ""), 'info', fsm.back.info));
        document.getElementById("play-icon").addEventListener("click", function () {
            var face = $(div).data("facing") == "back" ? "front" : "back";
            make_image(fsm, div, face);
            fill_info(_.assoc(fsm, 'name', fsm[face].name.replace("_", ""), 'info', fsm[face].info));
        });
        document.getElementById("loading-text").style.display = "none";
        document.getElementById("fusuma-zoom").style.visibility = "hidden";
    }


    function make_image (fsm, div, face) {
        div.style.backgroundImage = "url(" + thumbs.replace("small", "") + fsm[face].name + "/thumb.png)";
        div.style.backgroundSize = "cover";
        div.style.backgroundPosition = "center";
        div.style.width = "100%";
        $(div).data("facing", face);
        return div;
    }


    function isSmallScreen () {
        return fusuma_cv.getBoundingClientRect 
            ? fusuma_cv.getBoundingClientRect().height < 200
            : $(fusuma_cv).height();    }
})();